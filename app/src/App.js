import React, { Component } from 'react';
import './styles/common.less';
import { BrowserRouter } from 'react-router-dom';
import routes from './router/routerConfig';
import RouterView from './router/RouterView';
import './App.css';
export class App extends Component {
  render() {
    // eslint-disable-next-line prettier/prettier
    return (
      <div className="container">
        <BrowserRouter>
          <RouterView routes={routes}></RouterView>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
