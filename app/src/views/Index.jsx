// @ts-nocheck
import React, { Component } from 'react';
import RouterView from '../router/RouterView';
import Loading from '../component/Loading';
import { Icon } from 'react-vant';
import Indexmodule from '../views/style/index.module.less';
import { Indexroutes } from '../router/routerConfig';
import { Menu } from 'antd';
import { dark, light } from '../config/theme';
export class Index extends Component {
  state = {
    flag: false,
  };
  //切换路由
  handClickMenu({ key }) {
    this.props.history.push({
      pathname: Indexroutes[key].path,
    });
  }
  //主题切换
  handeClickFlag() {
    const { flag } = this.state;
    this.setState(
      {
        flag: !flag,
      },
      () => {
        //改变主题颜色
        window.less.modifyVars(!flag ? dark : light);
      }
    );
  }
  render() {
    let { routes } = this.props;
    let { flag } = this.state;
    return (
      <div className={flag ? Indexmodule.index2 : Indexmodule.index}>
        <header>
          <div className={Indexmodule.indexhead}>
            <div className={Indexmodule.nav_left}>
              <Loading className={Indexmodule.loading}></Loading>
              <Menu
                theme={!flag ? 'light' : 'dark'}
                className={Indexmodule.index_manu}
                mode="horizontal"
                onClick={this.handClickMenu.bind(this)}
                items={Indexroutes.map((item, index) => ({
                  ...item,
                  label: item.meta.title,
                  key: index,
                }))}
              />
            </div>
            <div className={Indexmodule.nav_right}>
              <select
                name="中文"
                id=""
                className={Indexmodule.nav_right_select}
              >
                <option
                  value="中文"
                  className={Indexmodule.nav_right_select_option}
                >
                  中文
                </option>
                <option value="Engilsh">英文</option>
              </select>
              <p
                className={Indexmodule.nav_right_off}
                onClick={this.handeClickFlag.bind(this)}
              >
                {flag ? '关灯' : '开灯'}
              </p>

              <Icon name="search" className={Indexmodule.nav_right_search} />

              <button className={Indexmodule.nav_rigint_login}>登录</button>
            </div>
          </div>
        </header>

        <section>
          <RouterView routes={routes} flag={flag}></RouterView>
        </section>
      </div>
    );
  }
}

export default Index;
