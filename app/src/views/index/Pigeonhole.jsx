/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import Containaer_right from '../../component/Containaer_right';
export class pigeonhole extends Component {
  render() {
    let { flag } = this.props;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          左盒子
        </div>
        <Containaer_right flag={flag}></Containaer_right>
      </div>
    );
  }
}

export default pigeonhole;
