import { lazy } from 'react';
export const Indexroutes = [
  {
    path: '/active',
    meta: {
      title: ' 文章 ',
    },
    component: lazy(() => import('../views/index/Active')),
    children: [
      {
        path: '/active',
        redirect: '/active/frontend',
      },
      {
        path: '/active/frontend',
        component: lazy(() => import('../views/index/Index/FrontEnd')),
      },
      {
        path: '/active/backend',
        component: lazy(() => import('../views/index/Index/BackEnd')),
      },
      {
        path: '/active/read',
        component: lazy(() => import('../views/index/Index/Read')),
      },
      {
        path: '/active/linux',
        component: lazy(() => import('../views/index/Index/Linux')),
      },
      {
        path: '/active/leetcode',
        component: lazy(() => import('../views/index/Index/LeetCode')),
      },
      {
        path: '/active/focusnews',
        component: lazy(() => import('../views/index/Index/FocusNews')),
      },
    ],
  },
  {
    path: '/pigeonhole',
    meta: {
      title: ' 归档 ',
    },
    component: lazy(() => import('../views/index/Pigeonhole')),
  },
  {
    path: '/knowledge',
    meta: {
      title: ' 知识小册 ',
    },
    component: lazy(() => import('../views/index/Knowledge')),
  },
  {
    path: '/message',
    meta: {
      title: ' 留言板 ',
    },
    component: lazy(() => import('../views/index/Message')),
  },
  {
    path: '/about',
    meta: {
      title: ' 关于 ',
    },
    component: lazy(() => import('../views/index/About')),
  },
];
const routes = [
  {
    path: '/',
    component: lazy(() => import('../views/Index')),
    children: [
      ...Indexroutes,
      {
        path: '/',
        redirect: '/active', // 重定向:重新指向其它path,会改变网址
      },
    ],
  },
];
export default routes;
