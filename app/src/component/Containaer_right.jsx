import React, { Component } from 'react';

export class Containaer_right extends Component {
  render() {
    let { flag } = this.props;
    return (
      <div className="container_right">
        <div
          className={
            flag ? 'container_right_toplist1' : 'container_right_toplist'
          }
        >
          右第一个小列表
        </div>
        <div
          className={
            flag ? 'container_right_bottomlist1' : 'container_right_bottomlist'
          }
        >
          右第二个小列表
        </div>
        <div className="container_right_bottom">小icon</div>
      </div>
    );
  }
}

export default Containaer_right;
