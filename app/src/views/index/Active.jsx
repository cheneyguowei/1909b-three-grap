// @ts-nocheck
/* eslint-disable no-undef */
/* eslint-disable react/jsx-pascal-case */
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import RouterView from '../../router/RouterView';
import Containaer_right from '../../component/Containaer_right';
export class Home extends Component {
  state = {
    flag: false,
  };
  render() {
    let { flag } = this.props;
    return (
      <div className="container_frame">
        <div className={flag ? 'container_left1' : 'container_left'}>
          <footer>
            <NavLink to="/active/frontend">前端</NavLink>
            <NavLink to="/active/backend">后端</NavLink>
            <NavLink to="/active/read">阅读</NavLink>
            <NavLink to="/active/linux">Linux</NavLink>
            <NavLink to="/active/leetcode">LeetCode</NavLink>
            <NavLink to="/active/focusnews">要闻</NavLink>
          </footer>
          <main>
            <RouterView routes={this.props.routes}></RouterView>
          </main>
        </div>
        <Containaer_right flag={flag}></Containaer_right>
      </div>
    );
  }
}

export default Home;
