import React, { Component, Suspense } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';

export class RouterView extends Component {
  render() {
    let { routes, flag } = this.props;
    // console.log(this.props.flag);
    console.log(this.props.routes);
    return (
      <Suspense fallback={'loading'}>
        <Switch>
          {routes &&
            routes.map((item, index) => {
              return (
                <Route
                  key={index}
                  path={item.path}
                  render={(props) => {
                    return item.redirect ? (
                      <Redirect to={item.redirect}></Redirect>
                    ) : (
                      <item.component
                        {...props}
                        routes={item.children}
                        flag={flag}
                      ></item.component>
                    );
                  }}
                ></Route>
              );
            })}
        </Switch>
      </Suspense>
    );
  }
}

export default RouterView;
