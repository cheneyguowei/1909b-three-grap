// @ts-nocheck
import React, { Component } from 'react';
import Indexmodule from '../views/style/index.module.less';
export class Loading extends Component {
  render() {
    return <div className={Indexmodule.loading}>Loading</div>;
  }
}

export default Loading;
